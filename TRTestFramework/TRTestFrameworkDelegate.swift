public protocol TRTestFrameworkDelegate {
    func onInitialized()
    func onError(error: TRError)
    func onException(exception: TRException)
    func onComplete(result: TRSuccess)
}
