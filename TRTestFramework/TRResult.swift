import Lottie
public protocol PTRResult : Encodable {
    var transactionId: String { get }
}

public struct TRResult : PTRResult, Encodable {
    public let transactionId: String
}

public struct TRSuccess : PTRResult, Encodable {
    public let transactionId: String
}

public struct TRError : PTRResult, Encodable {
    public var transactionId: String
    
    init(error: TRError) {
        transactionId = "transactionId"
    }
}

public struct TRException : PTRResult, Encodable {
    public let transactionId: String
}
